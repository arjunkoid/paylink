<?php
namespace Paylink\PlayLinkBundle\Service;

use Paylink\PlayLinkBundle\Service\SoapClient;
use Paylink\PlayLinkBundle\Service\SoapServiceInterface;


class SoapService implements SoapServiceInterface {

    protected $soapClient;

    public function __construct()
    {
        $this->soapClient = new SoapClient();
    }

    public function getPhysician($cmd, $param = null)
    {
        $repsone = $this->soapClient->sendRequest($cmd, $param);

        return $repsone;
    }



    public function getPhysicianAndSpeciality($cmd, $param = null)
    {
        $repsone = $this->soapClient->sendRequest($cmd, $param);

        return $repsone;
    }

    public function getAllSpecialty($cmd, $param = null)
    {
        $repsone = $this->soapClient->sendRequest($cmd, $param);

        return $repsone;
    }
}