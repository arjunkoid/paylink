<?php
namespace Paylink\PlayLinkBundle\Service;

interface SoapInterface {
    public function sendRequest($cmd, $param);
}