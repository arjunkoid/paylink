<?php
namespace Paylink\PlayLinkBundle\Service;

use Paylink\PlayLinkBundle\Service\SoapInterface;

class SoapClient implements  SoapInterface {
    const ENDPOINT = 'http://soap.paylinksolutions.pt/query.asmx?wsdl';

    public function sendRequest($cmd, $params)
    {

        $soapClient = new \SoapClient(self::ENDPOINT);

        $response = $soapClient->requestXML(['CMD' => $cmd,'PARAMS' => $params]);

        return $response;

    }
}