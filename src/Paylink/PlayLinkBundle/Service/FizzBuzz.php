<?php
namespace Paylink\PlayLinkBundle\Service;

/**
 * Class FizzBuzz
 * @package App\Service
 */

class FizzBuzz {

    const FIZZ = 'Fizz';
    const BUZZ = 'Buzz';

    /**
     * Get simple out put of the Fizz Buzz problem
     * @param int $minNumber
     * @param int $maxNumber
     *
     * @throws \Exception
     */
    public function getResult(int $minNumber, int $maxNumber)
    {
        if ($maxNumber < $minNumber) {
            throw new \Exception('Max Number should be greater than Min Value');
        }
        for ($i= $minNumber; $i <= $maxNumber; $i++) {
            //Check number is divisible by 3 and 5
            if ($i % 3 === 0 && $i % 5 === 0) {
                echo  self::FIZZ . self::BUZZ .  "<br>";
            } else if ($i % 3 === 0) { //Check number is devisible by 3
                echo self::FIZZ . "<br>";
            } else if ($i % 5 === 0) { //Check number is divisible by 5
                echo self::BUZZ . "<br/>";
            } else {
                echo $i . "<br/>";
            }
        }
    }
}

