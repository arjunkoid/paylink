<?php
namespace Paylink\PlayLinkBundle\Service;

Interface SoapServiceInterface {
    public function getPhysician($cmd);
    public function getPhysicianAndSpeciality($cmd);
    public function getAllSpecialty($cmd);
}