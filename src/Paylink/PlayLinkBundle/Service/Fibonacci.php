<?php
namespace Paylink\PlayLinkBundle\Service;

/**
 * Class Fibonacci
 * @package App\Service
 */
class Fibonacci {
    /**
     * @param int $n
     *
     * @return bool|int
     */
    public function generate(int $n) {
        if ($n < 0) {
            return false;
        } elseif ($n === 0) {
            return 0;
        } elseif ($n === 1 || $n ===2 ) {
            return 1;
        } else {
            return $this->generate($n-1) + $this->generate($n-2);
        }
    }
}

