<?php

namespace Paylink\PlayLinkBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Paylink\PlayLinkBundle\Service\FizzBuzz;
use Paylink\PlayLinkBundle\Service\Fibonacci;
use Paylink\PlayLinkBundle\Service\SoapService;

class DefaultController extends Controller
{


    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        var_dump($this->container->getParameter('soap_endpoint'));

        return $this->render('default/result.html.twig');
    }

    public function fizAction()
    {
        try {
            $fizzBuzzObj = new FizzBuzz();
            /**
             * Get Result of Fizz Buzz
             */
            $fizzBuzzObj->getResult(1, 20);
        } catch (\Exception $e) {
            $logger = $this->get('logger');
            $logger->error($e);
        }

        return $this->render('default/result.html.twig');
    }

    public function fibonacciAction()
    {
        try {
            $fibObj = new Fibonacci();
            $n = 10;
            for ($i=0; $i< $n; $i++) {
                echo $fibObj->generate($i) . " ";
            }

        }catch (\Exception $e) {
            $logger =  $this->get('logger');
            $logger->error($e);
        }

        return $this->render('default/result.html.twig');
    }


    public function listAction()
    {
        $obj = new SoapService();
        $obj->getPhysician(1, null);
        return $this->render('default/result.html.twig');
    }


}
